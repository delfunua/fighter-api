const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', function (req, res, next) {
  let users = UserService.getAll();
  if (!users) {
    res.status(404).json({message: 'Users not found'});
  }
  res.json(users);
});

router.get('/:id', function (req, res, next) {
  try {
    let id = req.params.id;
    let user = UserService.search({id});
    if (!user) {
      res.status(404).json({message: 'User not found'});
    }
    res.status(200).json(user);
  }
  catch (err) {
    res.status(400);
    next(err);
  }
});

router.post('/', createUserValid, (req, res, next) => {
  try {
    const {id, ...registerData} = req.body;
    let user = UserService.create(registerData);
    res.status(200).json({message: 'User created', ...user});
  }
  catch (err) {
    res.status(400);
    next(err);
  }
});

router.put('/:id', updateUserValid, (req, res, next) => {
  try {
    let user = UserService.update(req.params.id, req.body);
    res.status(200).json({message: 'User updated', ...user});
  }
  catch (err) {
    res.status(400);
    next(err);
  }
});

router.delete('/:id', function (req, res, next) {
  try {
    let user = UserService.delete(req.params.id);
    if (!user) {
      res.status(404).json({message: 'User not found'});
    }
    res.status(200).json({message: 'User deleted', ...user});
  }
  catch (err) {
    res.status(400);
    next(err);
  }
});

module.exports = router;
