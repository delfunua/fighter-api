const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
    	const user = AuthService.login(req.body);
    	if (!user) {
    		throw 'Wrong email/password';
    	}
        res.status(200);
        res.send(user);
        next();
    } catch (err) {
    	res.status(400);
        next(err);
    }
}, responseMiddleware);

module.exports = router;