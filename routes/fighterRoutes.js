const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', function (req, res, next) {
  console.log(res.data);
  let fighters = FighterService.getAll();
  res.json(fighters);
});

router.get('/:id', function (req, res, next) {
  try {
    let id = req.params.id;
    let fighter = FighterService.search({id});
    if (!fighter) {
      throw 'Fighter not found';
    }
    res.status(200).json(fighter);
  }
  catch (err) {
    res.status(400);
    next(err);
  }
});

router.post('/', createFighterValid, (req, res, next) => {
  try {
    const {id, ...registerData} = req.body;
    let fighter = FighterService.create(registerData);
    res.status(200).json({message: 'Fighter created', ...fighter});
  }
  catch (err) {
    res.status(400);
    next(err);
  }
});

router.put('/:id', updateFighterValid, (req, res, next) => {
  try {
    let {status, ...json} = FighterService.update(req.params.id, req.body);
    res.status(status).json(json);
  }
  catch (err) {
    res.status(400);
    next(err);
  }
});

router.delete('/:id', function (req, res, next) {
  try {
    const id = req.params.id;
    let {status, ...json} = FighterService.delete(req.params.id);
    res.status(status).json(json);
  }
  catch (err) {
    res.status(400);
    next(err);
  }
});

module.exports = router;
