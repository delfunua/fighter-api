const { UserRepository } = require('../repositories/userRepository');

class UserService {

  create(data) {
    const email = data.email;
    const phoneNumber = data.phoneNumber;
    if (this.search({email}))
    {
      return {error: true, message: 'User with associated email already exists', status: 404};
    }
    if (this.search({phoneNumber}))
    {
      return {error: true, message: 'User with associated phoneNumber already exists', status: 404};
    }
    const item = UserRepository.create(data);
    return {message: 'User created', ...item, status: 200};
  }

  update(id, dataToUpdate) {
    if (!this.search({id}))
    {
      return {error: true, message: 'User not found', status: 404};
    }
    const item = UserRepository.update(id, dataToUpdate);
    return {message: 'User updated', ...item, status: 200};
  }

  delete(id) {
    if (!this.search({id}))
    {
      return {error: true, message: 'User not found', status: 404};
    }
    const item = UserRepository.delete(id);
    return {message: 'User deleted', ...item, status: 200};
  }

  search(search) {
    const item = UserRepository.getOne(search);
    return item;
  }

  getAll() {
    return UserRepository.getAll();
  }
}

module.exports = new UserService();