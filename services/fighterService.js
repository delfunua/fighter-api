const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {

  create(data) {
    const name = data.name;
    if (this.search({name}))
    {
      throw 'Fighter already exists';
    }
    const item = FighterRepository.create(data);
    return {message: 'Fighter created', ...item, status: 200};
  }

  update(id, dataToUpdate) {
    if (!this.search({id}))
    {
      return {error: true, message: 'Fighter not found', status: 404};
    }
    const item = FighterRepository.update(id, dataToUpdate);
    return {message: 'Fighter updated', ...item, status: 200};
  }

  delete(id) {
    if (!this.search({id}))
    {
      return {error: true, message: 'Fighter not found', status: 404};
    }
    const item = FighterRepository.delete(id);
    return {message: 'Fighter deleted', ...item, status: 200};
  }

  search(search) {
    const item = FighterRepository.getOne(search);
    return item;
  }

  getAll() {
    return FighterRepository.getAll();
  }
}

module.exports = new FighterService();