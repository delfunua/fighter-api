exports.fighter = {
  "name": "^([A-Z][a-z]+\\s?)+$",
  "health": 100,
  "power": "^([1-9][0-9]{0,1}|100)$",
  "defense": "^([1-9]|10)$",
}
