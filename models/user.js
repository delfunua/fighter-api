exports.user = {
  firstName: '^([A-Z][a-z]+|[А-ЯІ][а-яі]+)$',
  lastName: '^([A-Z][a-z]+|[А-ЯІ][а-яі]+)$',
  email: '^[A-Za-z0-9_\\-\\.]+@gmail.com$',
  phoneNumber: '^\\+380\\d{9}$',
  password: '^.{3,}$'
}
