const express = require('express')
const cors = require('cors');
const bodyParser = require('body-parser')
require('./config/db');

const app = express()

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const routes = require('./routes/index');
routes(app);

app.use('/', express.static('./client/build'));

app.use(function (err, req, res, next) {
  console.log(err);
  res.json({error: true, message: err});
})

const port = 3050;
app.listen(port, () => {});

exports.app = app;