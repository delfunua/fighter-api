const { user } = require('../models/user');

const createUserValid = (req, res, next) => {
  let {id, ...model} = user;
  let data = req.body;

  try {
    if (req.body.id) {
      throw String.raw`'id' shouldn't be in request's body`;
    }
    Object.entries(model).forEach(([key, value]) => {
      if (typeof value !== 'string')
      {
        data[key] = value;
      }
    });
    if ( Object.keys(model).filter(key => !Object.keys(data).includes(key)).length > 0) { 
      throw 'Some fields are empty';
    }
    if ( Object.keys(data).filter(key => !Object.keys(model).includes(key)).length > 0 ) {
      throw 'Some fields are unacceptable';
    }
    for (let [key, expr] of Object.entries(model)) {
      if (!RegExp(expr).test(data[key])) {
        throw `User's ${key} property is not valid`;
      }
    }
    next();
  }
  catch (err) {
    res.status(400);
    next(err)
  }
}

const updateUserValid = (req, res, next) => {
  let {id, ...model} = user;
  let data = req.body;

  try {
    if (req.body.id) {
      throw String.raw`'id' shouldn't be in request's body`;
    }
    if ( Object.keys(data).filter(key => !Object.keys(model).includes(key)).length > 0 ) {
      throw 'Some fields are unacceptable';
    }
    Object.entries(model).forEach(([key, value]) => {
      if (typeof value !== 'string')
      {
        data[key] = value;
      }
    });
    for (let [key, value] of Object.entries(data)) {
      if (!RegExp(model[key]).test(value)) {
        throw `User's ${key} to create is not valid`;
      }
    }
    next();
  }
  catch (err) {
    res.status(400);
    next(err);
  }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;